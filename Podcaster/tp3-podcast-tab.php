<?php
    require_once('vendor/dg/rss-php/src/Feed.php');

    $url = "http://radiofrance-podcast.net/podcast09/rss_14312.xml";
    $rss = Feed::loadRss($url);

    //DEMO
    //====================================================
    // echo 'Title: ', $rss->title;
    // echo 'Description: ', $rss->description;
    // echo 'Link: ', $rss->url;

    // foreach ($rss->item as $item) {
    //     echo 'Title: ', $item->title;
    //     echo 'Link: ', $item->url;
    //     echo 'Timestamp: ', $item->timestamp;
    //     echo 'Description ', $item->description;
    //     echo 'HTML encoded content: ', $item->{'content:encoded'};
    // }
    //====================================================
?>

<html>
    <head>
        <Title>TP3 - RSS</Title>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1>TP 3 - RSS</h1>

        <table>
            <?php
                foreach ($rss->item as $item) {
                    // echo "==========================";
                    // echo "<pre>";
                    // var_dump($item);
                    // echo "</pre>";
                    $date = date('d-m-Y H:i:s', intval($item->timestamp[0]));
                    $audioURL = $item->enclosure['url'];
                    $duree = $item -> {"itunes:duration"};

                    echo "<tr>";
                    echo "<td> Date: ".date($date)."</td>";

                    echo "<td> <a href='$item->link' title='$item->description'>";
                    echo "Titre: ".$item->title;
                    echo "</a> </td>";

                    echo "<td> <audio 
                            controls
                            src = '$audioURL'>
                            Your browser does not support the <code>audio</code> element.
                        </audio> </td>";

                    echo "<td> Durée: $duree</td>";

                    echo "<td> <a href='$audioURL' download > Download </a> </td>";

                    echo "</tr>";
                }
            ?>
        </table>

        </ul>
    </body>
</html>

<style>
    td, th{
        padding: 1ex;
        border: 1px solid black;
    }

</style>