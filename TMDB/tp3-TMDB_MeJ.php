<?php
    //Questions 1 à 3 du TP, Mise en Jambe
    require_once('tp3-helpers.php');

    $urlcomponent = 'movie/550';
    $params = array(
        "language" => "fr"
    );

    $JSONdata = tmdbget($urlcomponent, $params);

    $data = json_decode($JSONdata);

    echo "\n";

    $homepage = $data->homepage;

?>

<html>
    <head>
        <Title>TMDB - TP3</Title>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1>Infos Films</h1>
        <ul>
            <li>Titre:  <?php echo "$data->title" ?></li>
            <li>Titre original: <?php echo "$data->original_title" ?></li>
            <li>Tagline:    <?php echo "$data->tagline" ?></li>
            <li>Description:    <?php echo "$data->overview" ?></li>
            <li>Lien vers TMDB: <a href="<?php echo $homepage?>">Lien</a></li>
        </ul>
    </body>
</html>