<!-- PAGE DEDIEE A L'AFFICHAGE D'UNE COLLECTION DE FILMS ET DE SES ACTEURS -->
<?php
    require_once('tp3-helpers.php');
    require_once('tp3-tools.php');

    //on récupère les informations sur la collection dont l'id est passée en paramètre
    //dans l'URL et récupère la partie exploitable
    $urlcomponent = 'collection/'.$_GET['id'];
    $data = api_get($urlcomponent);
    $result_tab = $data->parts;


    //On récupères les informations détaillées de chaque film de la collection
    //afin de manipuler une table unique et complète
    $params = array(
        "language" => "fr",
        "append_to_response" => "credits"
    );
    foreach ($result_tab as $key => $value) {
        $urlcomponent = 'movie/'.$value->id;
        $JSONdata = tmdbget($urlcomponent, $params);
        $dataMovies[$key] = json_decode($JSONdata);
    }

    //----------------------------------------------------

    //Renvoie true si le nom de l'acteur est déjà présent dans la table
    function searchActorName($name, $tab){
        foreach ($tab as $key => $value) {
            if ($value["name"] == $name){
                return $key;
            }
        }
        return FALSE;
    }

    //----------------------------------------------------
    //À partir de la table des films d'une collection, on construit une table
    //des acteurs ayant interprété un personnage à l'intérieur.
    $actors = array();
    $actors_size = 0;

    foreach ($dataMovies as $key => $value) {
        $credits = $value->credits;
        $cast = $credits->cast;

        foreach ($cast as $key2 => $value2) {
            $test = searchActorName($value2->name, $actors);
            if ($test === FALSE){
                $actors[$actors_size] = array(
                    "name" => $value2->name,
                    "role" => $value2->character,
                    "nb_movies" => 1,
                    "profile_path" => $value2->profile_path,
                    "person_id" => $value2->id
                );
                $actors_size++;
            }
            else {
                $actors[$test]["nb_movies"]++;
            }
        }
    }

?>

<html>
    <head>
        <Title>TMDB - Collection</Title>
        <meta charset="UTF-8">
    </head>
    <body>

        <a href="tp3-home.html"> Home </a>

        <?php

            echo "<h1> $data->name </h1>";

            htmlMovieTab($result_tab);
        ?>

        <h2> Cast </h2>

        <?php
            echo "<table>";
            foreach ($actors as $key => $value) {
                $name = $value["name"];
                $role = $value["role"];
                $nb_movies = $value["nb_movies"];
                $profile = "https://image.tmdb.org/t/p/w92".$value['profile_path'];
                $link = "tp3-actor.php?id=".$value['person_id']."&name=".$value['name'];

                echo "<tr>";
                    echo "<td> <a href='$link'> Nom:   $name </a></td>";
                    echo "<td>Rôle:   $role </td>";
                    echo "<td>Apparitions dans cette collection:   $nb_movies </td>";
                    echo "<td> <img src=\"$profile\"> </td>";
                echo "</tr>";
            }
            echo "</table>";
        ?>

</html>

<style>
    td, th{
        padding: 1ex;
        border: 1px solid black;
    }
</style>