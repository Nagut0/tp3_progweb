<!-- PAGE D'UN FILM -->
<?php
    require_once('tp3-helpers.php');
    require_once('tp3-tools.php');

    //on récupère les informations du film correspondant à
    //l'ID passé dans l'URL, en français ici
    $urlcomponent = 'movie/'.$_GET['id'];
    $params = array(
        "language" => "fr",
        "append_to_response" => "videos"
    );
    $dataFR = api_get($urlcomponent, $params);

    //idem après avoir vérifié la langue originel du film
    $params["language"] = $dataFR->original_language;
    $dataVO = api_get($urlcomponent, $params);

    //idem en anglais
    $params["language"] = "en";
    $dataEN = api_get($urlcomponent, $params);

    //Liens pour le contenu multimédia à intégrer dans la page
    $poster = "https://image.tmdb.org/t/p/w185".$dataFR->poster_path;
    $link = "https://www.themoviedb.org/".$urlcomponent;
    $videoLink = "https://www.youtube.com/embed/".$dataFR->videos->results[0]->key;


    //On affiche la collection associée à un film uniquement si elle existe
    $collectionSet = false;

    if (isset($dataFR->belongs_to_collection)){
        $idCollection = $dataFR->belongs_to_collection->id;
        $nameCollection = $dataFR->belongs_to_collection->name;
        $collectionSet = True;
    }

?>

<html>
    <head>
        <Title>TMDB - Movie</Title>
        <meta charset="UTF-8">
    </head>
    <body>

        <a href="tp3-home.html"> Home </a>

        <?php
            echo "<h1> $dataFR->title </h1>";

            if ($collectionSet){
                echo "<h2>Dans la collection: <a href='tp3-TMDB-collection.php?id=$idCollection'> $nameCollection</a></h2>";
            }
        ?>

        <img src="<?php echo "$poster"?>">

        <a href="tp3-actorMovie.php?id=<?php echo $_GET['id']."&name=$dataFR->title"?>" > Liste des acteurs </a>


        <table>
            
            <tr>
                <td>VO</td>
                <td>EN</td>
                <td>FR</td>
            </tr>
            <tr>
                <td>Title:  <?php echo "$dataVO->title"?></td>
                <td>Title:  <?php echo "$dataEN->title"?></td>
                <td>Titre:  <?php echo "$dataFR->title"?></td>
            </tr>
            <tr>
                <td>Original title:  <?php echo "$dataVO->original_title"?></td>
                <td>Original title:  <?php echo "$dataEN->original_title"?></td>
                <td>Titre original:  <?php echo "$dataFR->original_title"?></td>
            </tr>
            <tr>
                <td>Tagline:  <?php echo "$dataVO->tagline"?></td>
                <td>Tagline:  <?php echo "$dataEN->tagline"?></td>
                <td>Tagline:  <?php echo "$dataFR->tagline"?></td>
            </tr>
            <tr>
                <td>Description:  <?php echo "$dataVO->overview"?></td>
                <td>Description:  <?php echo "$dataEN->overview"?></td>
                <td>Description:  <?php echo "$dataFR->overview"?></td>
            </tr>
            <tr>

                <td><a href="<?php echo $link?>">Link to the movie's page</a></td>
                <td><a href="<?php echo $link?>">Link to the movie's page</a></td>
                <td><a href="<?php echo $link?>">Lien vers la page du film</a></td>
            </tr>

        </table>

        <iframe width="560" height="315" src="<?php echo"$videoLink" ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </body>
</html>

<style>
    td, th{
        padding: 1ex;
        border: 1px solid black;
    }
</style>