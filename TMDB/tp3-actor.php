<!-- PAGE DEDIEE AU CASTING D'UN ACTEUR -->
<?php  
    require_once('tp3-helpers.php');
    require_once('tp3-tools.php');

    //on récupère tous les films dans lesquels a joué l'acteur dont
    //l'ID est passé dans l'URL
    $urlcomponent = "person/".$_GET['id']."/movie_credits";
    $params = array(
        "language" => "fr",
    );
    $data = api_get($urlcomponent, $params);
    
?>

<html>
    <head>
        <Title>TMDB - Actor</Title>
        <meta charset="UTF-8">
    </head>
    <body>
        <a href="tp3-home.html"> Home </a>

        <?php
            echo "<h1>Casting de ".$_GET['name'];
            echo "</h1>";

            echo "<table>";
            foreach ($data->cast as $key => $value) {
                $poster = "https://image.tmdb.org/t/p/w92".$value->poster_path;

                echo "<tr>";
                    echo "<td> <a href='tp3-TMDB-movie.php?id=$value->id' >Titre:   $value->title </a></td>";
                    echo "<td>Rôle:   $value->character</td>";
                    echo "<td> <img src=\"$poster\"> </td>";
                echo "</tr>";
            }
            echo "</table>";
        ?>
        
    </body>
</html>

<style>
    td, th{
        padding: 1ex;
        border: 1px solid black;
    }
</style>