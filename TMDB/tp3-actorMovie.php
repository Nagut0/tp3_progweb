<!-- LISTE DES ACTEURS D'UN FILM SANS COLLECTION (SANS COMPTAGE DES APPARITIONS) -->
<?php
    require_once('tp3-helpers.php');
    require_once('tp3-tools.php');

    //on récupère les informations sur le film dont l'id est passée en paramètre
    //dans l'URL et récupère la partie exploitable
    $urlcomponent = 'movie/'.$_GET['id'].'/credits';
    $data = api_get($urlcomponent);
    $actors = $data->cast;
?>

<html>
    <head>
        <Title>TMDB - Acteurs</Title>
        <meta charset="UTF-8">
    </head>
    <body>

        <a href="tp3-home.html"> Home </a>

        <?php

            echo "<h1> ".$_GET['name']." </h1>";

        ?>

        <h2> Cast </h2>

        <?php
            echo "<table>";
            foreach ($actors as $key => $value) {
                $name = $value->name;
                $role = $value->character;
                $profile = "https://image.tmdb.org/t/p/w92".$value->profile_path;
                $link = "tp3-actor.php?id=".$value->id."&name=".$value->name;

                echo "<tr>";
                    echo "<td> <a href='$link'> Nom:   $name </a></td>";
                    echo "<td>Rôle:   $role </td>";
                    echo "<td> <img src=\"$profile\"> </td>";
                echo "</tr>";
            }
            echo "</table>";
        ?>

</html>

<style>
    td, th{
        padding: 1ex;
        border: 1px solid black;
    }
</style>