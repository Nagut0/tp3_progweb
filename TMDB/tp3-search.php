<!-- PAGE DEDIEE AUX RECHERCHES DE FILM -->
<?php
    require_once('tp3-helpers.php');
    require_once('tp3-tools.php');

    //on récupère la liste des films correspondant à la recherche
    //passée en paramètre et on extrait la partie exploitable
    $urlcomponent = "search/movie";
    $params = array (
        "language" => "fr",
        "query" => $_GET['movieQuery']
    );
    $data = api_get($urlcomponent, $params);
    $result_tab = $data->results;

?>

<html>
    <head>
        <Title>TMDB - Search</Title>
        <meta charset="UTF-8">
    </head>
    <body>
        <a href="tp3-home.html"> Home </a>

        <?php
            echo "<h1>Films correspondants à la recherche: ".$_GET['movieQuery'];
            echo "</h1>";

            htmlMovieTab($result_tab);
        ?>

</body>
</html>

<style>
    td, th{
        padding: 1ex;
        border: 1px solid black;
    }
</style>