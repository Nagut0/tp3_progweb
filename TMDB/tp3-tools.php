<!-- FICHIER ANNEXE AVEC QUELQUES OUTILS -->
<?php

    //fonction retournant le résultat d'une requête API exploitable en PHP
    function api_get($urlcomponent, $params=null) {
        $JSONdata = tmdbget($urlcomponent, $params);
        $data = json_decode($JSONdata);

        return $data;
    }

    //fonction d'afichage par défaut d'une liste de films
    function htmlMovieTab($result_tab){
        echo "<table>";
            foreach ($result_tab as $key => $value) {
                $poster = "https://image.tmdb.org/t/p/w92".$value->poster_path;

                echo "<tr>";
                    echo "<td>Identifiant:   $value->id</td>";
                    if (isset($value->release_date)){
                        echo "<td>Date de Sortie:   $value->release_date</td>";
                    }
                    echo "<td> <a href='tp3-TMDB-movie.php?id=$value->id' >Titre:   $value->title </a></td>";
                    echo "<td> <img src=\"$poster\"> </td>";
                echo "</tr>";
            }
            echo "</table>";
    }
?>